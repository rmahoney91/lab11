#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	char line[50];
	int count = 0;
	int init = 10;
	// TODO
	// Allocate memory for an array of strings (arr).
	char **arr = malloc(init * sizeof(char*)); //Allocate a hard value for now
	// Read the dictionary line by line.
	while(fgets(line, 50, in) != NULL)
	{
		// Allocate memory for the string (str).
		char *s = malloc(50 * sizeof(char));
		
		char *nl = strchr(line, '\n');
		if(nl != NULL)
		{
			*nl = '\0';
		}
		// Copy each line into the string (use strcpy).
		strcpy(s, line);
		
		// Attach the string to the large array (assignment =).
		arr[count] = s;
		count++;
		// Expand array if necessary (realloc).
		arr = realloc(arr, (count + init) * sizeof(char*));
	}
	
	// The size should be the number of entries in the array.
	*size = count;
	
	//for(int i = 0; i < count; i++) for debugging purposes only
	//{
	//	printf("%d %s\n", count, arr[i]);
		
	//}
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}